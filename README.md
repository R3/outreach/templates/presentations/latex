
# LCSB LaTeX beamer template

## MOVED TO https://gitlab.com/uniluxembourg/lcsb/common/beamerthemelcsb

This is a simple work-alike of the official LCSB slide templates, heavily
inspired by the Metropolis theme.

Clone this and type `make`, if your TeX distribution is okay, the example
`slides.tex` should get compiled to `slides.pdf`.

Proper documentation will be written once this gets used a bit and all rough
edges are cleaned up; in the meantime it is likely better to just follow the
example layouts in `slides.tex`.

Open issues/send merge requests in case of any ideas.

# How to generate the PDF file using Gitlab CI/CD?
Go to "CI/CD" tab on the left-hand side. Find the button to start "build:pdf file" task, and click it.
After a few minutes the task should finish, and you'll find on the right-hand-side an option to download the "Arftifacts" - an archive containing the generated PDF file.
