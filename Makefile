LATEXMKOPTS=-lualatex #change to -pdf for forcing pdflatex
LATEXMK=latexmk $(LATEXMKOPTS)

slides.pdf: $(wildcard *.tex) $(wildcard theme/*.sty) $(wildcard media/*)
	$(LATEXMK) slides

clean:
	$(LATEXMK) -C slides
